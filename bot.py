import praw
import pdb
import re
import os


reddit = praw.Reddit('BHARATBOT')
subreddit = reddit.subreddit('worldnews')

if not os.path.isfile('posts_replied_to.txt'):
    posts_replied_to = []
else:
    with open('posts_replied_to.txt', 'r') as fp:
        posts_replied_to = list(filter(None, fp.read().split('\n')))



for submission in subreddit.hot(limit=10):
    if (submission.id not in posts_replied_to) and re.search("Donald", submission.title, re.IGNORECASE):
        print("Bharat bot replying to: " + submission.title)
        submission.reply("Sad!!")
        posts_replied_to.append(submission.id)

with open('posts_replied_to.txt', 'w') as fp:
    for post_id in posts_replied_to:
        fp.write(post_id + '\n')


